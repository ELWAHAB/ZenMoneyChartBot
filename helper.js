const {JsonDB} = require("node-json-db");
const {Config} = require("node-json-db/dist/lib/JsonDBConfig");

const LangDataBase = new JsonDB(new Config("LangDataBase", true, false, '/'));
const TokenDataBase = new JsonDB(new Config("TokenDataBase", true, false, '/'));

module.exports = {
    getLangCode: function (userId, message) {

        let lang;
        TokenDataBase.reload();

        if (!TokenDataBase.exists(`/${userId}/language_code`)) {
            lang = message.from.language_code;
        } else {
            lang = TokenDataBase.getData(`/${userId}/language_code`);
        }

        const existLang = LangDataBase.exists(`/${lang}`);
        if (!existLang) {
            return 'ua';
        }

        return lang;
    },

    getLangMessage: function (langCode, query) {
        return LangDataBase.getData(`/${langCode}${query}`);
    }
};