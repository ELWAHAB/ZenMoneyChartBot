const request = require('request-promise-native');
const BaseZenMoney = require('./BaseZenMoney');
const {getLangMessage} = require("./helper");
const moment = require("moment");
const QuickChart = require("quickchart-js");

class ZenMoney extends BaseZenMoney {

    async getAccounts() {
        await this.getDiff();

        const accounts = [];

        for (const account of this.zenmoneyData.account) {
            accounts.push({
                title: account.title,
                type: account.type,
                balance: account.balance,
            });
        }

        return accounts;
    }

    async transferByRules(rules) {
        const transactions = [];

        if (!rules.length) {
            console.log('Not found rules');

            return false;
        }

        for (const rule of rules) {
            const result = await this.transferMoneyByRule(rule);

            if (!result) {
                continue;
            }

            transactions.push(...result);
        }

        return transactions;
    }

    async transferMoneyByRule(rule = false) {
        if (!rule) {
            console.log('Not found rules');

            return false;
        }

        await this.getDiff();

        const incomeBankAccount = this.findEntity(this.zenmoneyData.account, rule.incomeBankAccount.name, 'title');
        const outcomeBankAccount = this.findEntity(this.zenmoneyData.account, rule.outcomeBankAccount.name, 'title');

        const transactions = this.findEntity(this.zenmoneyData.transaction, rule.rule, rule.column, (transaction) => {
            return transaction.incomeAccount === transaction.outcomeAccount;
        }, true);

        if (!transactions.length) {
            console.log('Not found transactions')

            return false;
        }

        for (const transactionEntity of transactions) {
            if (!transactionEntity) {
                continue;
            }

            transactionEntity.incomeAccount = incomeBankAccount.id;
            transactionEntity.outcomeAccount = outcomeBankAccount.id;
            transactionEntity.comment += ' (Changed by API)';

            transactionEntity.income = transactionEntity.outcome;

            await this.updateTransaction(transactionEntity);
        }

        console.log('Transferred...');

        return transactions;
    }

    async updateTransaction(transaction, serverTimestamp = 0) {
        const options = {
            currentClientTimestamp: (new Date()).getTime() / 1000,
            serverTimestamp: transaction.changed - 10 - Math.floor(Math.random() * 100),
            transaction: [],
        };

        transaction.changed += 10;
        options.transaction.push(transaction);

        return this.baseRequest(
            '/v8/diff/',
            'POST',
            options
        );
    }

    async generateChart(langCode = 'ua', defaultDate = 7) {
        const chart = {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                title: {
                    display: true,
                    text: getLangMessage(langCode, '/chart/title').replace(':defaultDate', defaultDate)
                }
            }
        };

        const data = {};
        const diff = await this.getDiff();

        const transactions = diff.transaction;
        for (const transaction of transactions) {
            if (transaction.deleted) {
                continue;
            }

            if (moment(transaction.date) < moment().subtract(defaultDate, "days")) {
                continue;
            }

            if (!data[transaction.date]) {
                data[transaction.date] = {
                    income: 0,
                    outcome: 0,
                };
            }

            if (transaction.income !== 0 && transaction.outcome === 0) {
                data[transaction.date].income += transaction.income;

                continue;
            }

            if (transaction.income === 0 && transaction.outcome !== 0) {
                data[transaction.date].outcome += transaction.outcome;
            }
        }


        const incomeValues = [];
        const outcomeValues = [];
        for (const label of Object.keys(data).sort()) {
            incomeValues.push(data[label].income);
            outcomeValues.push(data[label].outcome);
        }

        chart.data.labels = Object.keys(data).sort();

        chart.data.datasets.push({label: getLangMessage(langCode, '/chart/labels/income'), data: incomeValues, backgroundColor: 'green'});
        chart.data.datasets.push({label: getLangMessage(langCode, '/chart/labels/outcome'), data: outcomeValues, backgroundColor: 'red'});

        const myChart = new QuickChart();
        myChart.setConfig(chart);

        return myChart.getUrl();
    }
}

module.exports = ZenMoney;