const {getLangMessage, getLangCode} = require('./helper');
const ZenMoneyClient = require('./ZenMoney');
const moment = require('moment');
const { JsonDB } = require('node-json-db');

const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
const TelegramBot = require('node-telegram-bot-api');

const {TELEGRAM_BOT_TOKEN, AVAILABLE_LANG, TELEGRAM_ADMIN_ID} = require('./credentials');

const bot = new TelegramBot(TELEGRAM_BOT_TOKEN, {polling: true});

const LangDataBase = new JsonDB(new Config("LangDataBase", true, false, '/'));
const TokenDataBase = new JsonDB(new Config("TokenDataBase", true, false, '/'));

(async () => {
    console.log('Start');

    bot.onText(/\/token (.+)/, async (msg, match) => {
        console.log('Token command')

        const chatId = msg.chat.id;
        const token = match[1];
        const langCode = getLangCode(chatId, msg);

        try {
            await bot.sendMessage(chatId, getLangMessage(langCode, '/token/waite_for_check_token'));

            TokenDataBase.push(`/${chatId}/token`, token);

            const credentials = {
                accessToken: token
            };
            const ZenMoney = new ZenMoneyClient(credentials);
            await ZenMoney.authorize();
            await ZenMoney.getDiff();

            TokenDataBase.save();
            await bot.sendMessage(chatId, getLangMessage(langCode, '/token/active_token'));
        } catch (error) {
            await bot.sendMessage(chatId, getLangMessage(langCode, '/token/wrong_token').replace(':token', token));
        }
    });

    bot.onText(/\/token/, async (msg, match) => {
        const chatId = msg.chat.id;
        const langCode = getLangCode(chatId, msg);

        if (msg.text.match(/\/token (.+)/)) {
            return;
        }

        await bot.sendMessage(chatId, getLangMessage(langCode, '/token/wrong_token').replace(':token', ' '));
    });

    bot.onText(/\/chart/, async (msg, match) => {
        const chatId = msg.chat.id;
        const langCode = getLangCode(chatId, msg);

        if (msg.text.match(/\/chart (.+)/)) {
            return;
        }

        await bot.sendMessage(chatId, getLangMessage(langCode, '/chart/exception/count_days'));
    });

    bot.onText(/\/chart (.+)/, async (msg, match) => {
        console.log('Chart command')

        const chatId = msg.chat.id;
        const langCode = getLangCode(chatId, msg);

        try {
            const countDays = parseInt(match[1]);

            if (countDays <= 0) {
                await bot.sendMessage(chatId, getLangMessage(langCode, '/chart/exception/count_days'));

                return;
            }

            const existToken = TokenDataBase.exists(`/${chatId}/token`);
            if (!existToken) {
                await bot.sendMessage(chatId, getLangMessage(langCode, '/token/token_not_exist'));

                return;
            }

            await bot.sendMessage(chatId, getLangMessage(langCode, '/chart/waite_for_chart'));

            const credentials = {
                accessToken: TokenDataBase.getData(`/${chatId}/token`)
            };
            const ZenMoney = new ZenMoneyClient(credentials);
            await ZenMoney.authorize();

            const chartUrl = await ZenMoney.generateChart(langCode, countDays);
            await bot.sendPhoto(chatId, chartUrl);
        } catch (e) {
            TokenDataBase.push(`/${chatId}/exception/${moment.now()}`, e.message + "\n" + e.stack);
            TokenDataBase.save();

            await bot.sendMessage(chatId, getLangMessage(langCode, '/exception/some_happend_wrong'));
        }
    });

    bot.onText(/\/donate/, async (msg, match) => {
        console.log('Donate command')

        const chatId = msg.chat.id;
        const langCode = getLangCode(chatId, msg);

        try {
            await bot.sendMessage(chatId, getLangMessage(langCode, '/donate'));
        } catch (e) {
            TokenDataBase.push(`/${chatId}/exception/${moment.now()}`, e.message + "\n" + e.stack);
            TokenDataBase.save();

            await bot.sendMessage(chatId, getLangMessage(langCode, '/exception/some_happend_wrong'));
        }
    });

    bot.onText(/\/lang (.+)/, async (msg, match) => {
        console.log('Lang command')

        const chatId = msg.chat.id;
        const langCode = getLangCode(chatId, msg);
        const newLangCode = match[1];

        try {
            if (!newLangCode) {
                await bot.sendMessage(chatId, getLangMessage(langCode, '/lang/not_found_lang'));

                return;
            }

            if (!AVAILABLE_LANG.includes(newLangCode)) {
                await bot.sendMessage(chatId, getLangMessage(langCode, '/lang/not_found_lang'));

                return;
            }

            TokenDataBase.push(`/${chatId}/language_code`, newLangCode);
            TokenDataBase.save();
            await bot.sendMessage(chatId, getLangMessage(newLangCode, '/lang/changed'));
        } catch (e) {
            TokenDataBase.push(`/${chatId}/exception/${moment.now()}`, e.message + "\n" + e.stack);
            TokenDataBase.save();

            await bot.sendMessage(chatId, getLangMessage(langCode, '/exception/some_happend_wrong'));
        }
    });

    bot.onText(/\/lang/, async (msg, match) => {
        const chatId = msg.chat.id;
        const langCode = getLangCode(chatId, msg);

        if (msg.text.match(/\/lang (.+)/)) {
            return;
        }

        await bot.sendMessage(chatId, getLangMessage(langCode, '/lang/not_found_lang'));
    });

    bot.onText(/\/start/, async (msg, match) => {
        const chatId = msg.chat.id;
        const langCode = getLangCode(chatId, msg);

        await bot.sendMessage(chatId, getLangMessage(langCode, '/start'));
    });

    bot.onText(/\/list/, async (msg, match) => {
        console.log('List command')

        const chatId = msg.chat.id;
        const langCode = getLangCode(chatId, msg);

        try {
            await bot.sendMessage(chatId, getLangMessage(langCode, '/list'));
        } catch (e) {
            TokenDataBase.push(`/${chatId}/exception/${moment.now()}`, e.message + "\n" + e.stack);
            TokenDataBase.save();

            await bot.sendMessage(chatId, getLangMessage(langCode, '/exception/some_happend_wrong'));
        }
    });

    bot.on('message', (msg) => {
        try {
            const chatId = msg.chat.id;
            let lang = TokenDataBase.exists(`/${chatId}/language_code`);

            if (lang) {
                return;
            }

            lang = msg.from.language_code;

            const existLang = LangDataBase.getData(`/${lang}`);

            if (!existLang) {
                lang = 'ua';
            }

            TokenDataBase.push(`/${chatId}/language_code`, lang);
            TokenDataBase.save();

            const message = "Вітаю 🥳🥳🥳!\n" +
                "У нас новий користувач:\n\n" +
                `UserId: ${msg.from.id}\n` +
                `First Name: ${msg.from.first_name}\n` +
                `Last Name: ${msg.from.last_name ? msg.from.last_name : 'Not found'}\n` +
                `Username: ${msg.from.username ? ('@' + msg.from.username) : 'Not found'}\n` +
                `Language Code: ${msg.from.language_code ? msg.from.language_code : 'Not found'}`;

            bot.sendMessage(TELEGRAM_ADMIN_ID, message);
        } catch (error) {
            console.log({
                error
            })
        }
    });
})()