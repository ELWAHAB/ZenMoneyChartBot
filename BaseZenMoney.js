const conf = require("./credentials");
const request = require("request-promise-native");
const fs = require("fs");
const moment = require("moment");

class BaseZenMoney {

    urlBase = 'https://api.zenmoney.ru';
    urlAuth = 'https://api.zenmoney.ru/oauth2/authorize/';
    urlToken = 'https://api.zenmoney.ru/oauth2/token';
    urlDiff = 'https://api.zenmoney.ru/v8/diff';

    accessToken;

    credentials;
    urlRedirect;
    tokens;
    zenmoneyData;

    constructor(credentials) {
        this.credentials = credentials;
        this.urlRedirect = credentials.urlRedirect;
    }

    async authorize(force = false) {
        try {
            if (this.credentials.accessToken && !force) {
                console.log('Token exists');
                this.tokens = {
                    access_token: this.credentials.accessToken
                };

                return;
            }

            const cookies = await this.getCookies();
            const code = await this.getCode(cookies);
            this.tokens = await this.getTokens(code);
        } catch (err) {
            throw new Error(err);
        }
    }

    async getCookies() {
        try {
            const options = {
                method: 'GET',
                url: this.urlAuth,
                qs: {
                    response_type: 'code',
                    client_id: conf.consumerKey,
                    redirect_uri: this.urlRedirect
                },
                gzip: true,
                resolveWithFullResponse: true
            };

            const response = await request(options);

            const cookie = response.headers['set-cookie']['0'].split(';')[0];

            if (cookie.length == 0) {
                throw new Error(options.url + ' > cookie.length == 0');
            }

            const j = request.jar();
            j.setCookie(request.cookie(cookie), options.url);

            return j;

        } catch (err) {
            throw new Error(err);
        }
    }

    async getCode(cookies) {
        try {
            const options = {
                method: 'POST',
                url: this.urlAuth,
                qs: {
                    response_type: 'code',
                    client_id: conf.consumerKey,
                    redirect_uri: this.urlRedirect
                },
                form: {
                    username: conf.username,
                    password: conf.password,
                    auth_type_password: 'Sign in',
                },
                gzip: true,
                jar: cookies
            };

            return await request(options);
        } catch (err) {
            if (err.statusCode == 302) {
                const urlParams = err.message.split('?')[1].split('&');
                let code = '';

                for (let u of urlParams) {
                    if (u.indexOf('code') == 0) {
                        code = u.replace('code=', '');
                    }
                }

                if (code == '') {
                    throw new Error('getCode > code not found');
                }

                return code;
            }

            throw new Error(err);
        }
    }

    async getTokens(code) {
        try {
            const options = {
                method: 'POST',
                url: this.urlToken,
                form: {
                    grant_type: 'authorization_code',
                    client_id: conf.consumerKey,
                    client_secret: conf.consumerSecret,
                    code: code,
                    redirect_uri: this.urlRedirect
                },
                gzip: true,
                json: true
            };

            return await request(options);
        } catch (err) {
            throw new Error(err);
        }
    }

    async getDiff(serverTimestamp = 0) {
        try {
            const options = {
                currentClientTimestamp: (new Date()).getTime() / 1000,
                serverTimestamp: serverTimestamp
            };

            return await this.baseRequest('/v8/diff', 'POST', options);
        } catch (err) {
            throw new Error(err);
        }
    }

    async baseRequest(url, method = 'GET', json = {}) {
        try {
            const endpoint = this.urlBase + url;
            console.log({endpoint});

            const options = {
                method: method,
                url: endpoint,
                json: json,
                gzip: true,
                auth: {
                    'bearer': this.tokens.access_token
                }
            };

            this.zenmoneyData = await request(options);

            return this.zenmoneyData;
        } catch (err) {
            throw new Error(err);
        }
    }

    findEntity(entities, value, column = 'name', callback = null, isPlural = false) {
        let result = null;

        if (isPlural) {
            result = [];
        }

        entities.forEach((entity) => {
            if (!entity[column]) {
                return;
            }

            if (entity[column].indexOf(value) < 0) {
                return;
            }

            if (callback && !callback(entity)) {
                return;
            }

            if (isPlural) {
                result.push(entity);
            } else {
                result = entity;
            }
        });

        return result;
    }

    save(ext, body) {
        let dir = process.cwd() + '/export';

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }

        dir += '/' + ext;

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }

        let fileName = 'zen_' + moment().format('YYYY-MM-DD-HHmmss') + '.' + ext;

        let filePath = dir + '/' + fileName;

        fs.writeFileSync(filePath, body);

        console.log(fileName + ' saved!'); // eslint-disable-line no-console
    }
}

module.exports = BaseZenMoney;