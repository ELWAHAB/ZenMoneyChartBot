require('dotenv').config();

module.exports = {
    "name_project": process.env.NAME_PROJECT,
    "email": process.env.EMAIL,
    "site": process.env.SITE,
    "urlRedirect": process.env.URL_REDIRECT,
    "username": process.env.ZEN_USERNAME,
    "password": process.env.ZEN_PASSWORD,
    "consumerKey": process.env.CONSUMER_KEY,
    "consumerSecret": process.env.CONSUMER_SECRET,
    "accessToken": process.env.ACCESS_TOKEN,
    TELEGRAM_BOT_TOKEN: process.env.TELEGRAM_BOT_TOKEN,
    TELEGRAM_ADMIN_ID: process.env.TELEGRAM_ADMIN_ID,
    AVAILABLE_LANG: ['en', 'ua', 'ru']
}